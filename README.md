## Langkah instalasi

- Clone project ini
- Pastikan docker sudah berjalan
- Jalankan perintah berikut untuk build image dan run container:
```
docker-compose up -d --build
```
- Kemudian jalankan juga perintah berikut untuk migrasi basis data
```
docker-compose exec django python manage.py makemigrations
docker-compose exec django python manage.py migrate
```
- Setelah semua proses berhasil, buka web browser untuk mengecek aplikasi berjalan
- Untuk mengimport dummy data jalankan juga perintah berikut
```
docker-compose exec django python importer2.py
```