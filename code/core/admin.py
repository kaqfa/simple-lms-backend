from django.contrib import admin
from .models import Course
from .models import CourseContent

class CourseContentInline(admin.TabularInline):
    model = CourseContent
    fields = ('name', 'description', 'video_url')
    extra = 1
    
class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'price')
    inlines = [CourseContentInline]

admin.site.register(Course, CourseAdmin)


