from typing import List
from ninja import NinjaAPI, Query
from ninja.pagination import paginate, PageNumberPagination
from django.db.models import Count

from .schemas import CalcIn, CourseContentSchema, CourseOut, CourseFilter, UserOut
from .schemas import Register, CommentIn, CourseMember, CourseMemberOut, DetailCourseOut

from .models import Course, User, CourseMember, CourseContent, Comment

from ninja_simple_jwt.auth.views.api import mobile_auth_router
from ninja_simple_jwt.auth.ninja_auth import HttpJwtAuth

apiv1 = NinjaAPI()
apiv1.add_router("/auth/", mobile_auth_router)
apiAuth = HttpJwtAuth()

@apiv1.get('hello/')
def helloApi(request):
    return "Menyala abangkuh ..."

@apiv1.post('hello/')
def helloPost(request):
    if 'nama' in request.POST:
        return f"Selamat menikmati ya {request.POST['nama']}"
    return "Selamat tinggal dan pergi lagi"

@apiv1.put('users/{id}')
def userUpdate(request, id:int):
    return f"User dengan id {id} Nama aslinya adalah Herdiono kemudian diganti menjadi {request.body}"

@apiv1.delete('users/{id}')
def userDelete(request, id:int):
    return f"Hapus user dengan id: {id}"

@apiv1.get('calc/{nil1}/{opr}/{nil2}')
def calculator(request, nil1:int, opr:str, nil2:int):
    hasil = nil1 + nil2
    if opr == '-':
        hasil = nil1 - nil2
    elif opr == 'x':
        hasil = nil1 * nil2
    
    return {'nilai1': nil1, 'nilai2': nil2, 'operator': opr, 'hasil': hasil}


@apiv1.post('calc')
def postCalc(request, skim : CalcIn):
    skim.hasil = skim.calcHasil()
    return skim


@apiv1.post('register/', response=UserOut)
def register(request, data:Register):
    newUser = User.objects.create_user(username=data.username, 
                                password=data.password, 
                                email=data.email,
                                first_name=data.first_name, 
                                last_name=data.last_name)
    return newUser


@apiv1.post('course/{id}/enroll/', auth=apiAuth, response=CourseMemberOut)
def courseEnrollment(request, id:int):
    user_id = User.objects.get(pk=request.user.id)
    course = Course.objects.get(pk=id)

    enrollment = CourseMember.objects.create(user_id=user_id, course_id=course)
    return enrollment


@apiv1.get('mycourses/', auth=apiAuth, response=List[CourseMemberOut])
def getMyCourses(request):
    user_id = User.objects.get(pk=request.user.id)
    mycourses = CourseMember.objects.filter(user_id=user_id)\
                            .select_related('course_id', 'user_id')
    return mycourses


@apiv1.post('comments/', auth=apiAuth)
def postComment(request, data:CommentIn):
    user_id = User.objects.get(pk=request.user.id)
    content_id = CourseContent.objects.filter(id=data.content_id).first()
    coursemember = CourseMember.objects.filter(user_id=user_id, course_id=content_id.course_id)

    if coursemember.exists():
        Comment.objects.create(comment=data.comment, user_id=user_id, content_id=content_id)
        return "berhasil"
    else:
        return "tidak boleh komentar di sini"


@apiv1.get('courses/', response=List[DetailCourseOut], auth=apiAuth)
@paginate(PageNumberPagination, page_size=5)
def listAllCourse(request, filters: CourseFilter=Query(...)):
    """Mendapatkan semua data course yang sudah tersimpan di DB
    """
    courses = Course.objects.all()
    courses = filters.filter(courses)
    
    # Annotate each course with the number of members and contents
    courses = courses.annotate(
        num_members=Count('coursemember'),
        num_contents=Count('coursecontent')
    )
    
    return courses

@apiv1.get('courses/{course_id}/contents', response=List[CourseContentSchema], auth=apiAuth)
def list_course_contents(request, course_id: int):
    """
    Mendapatkan semua data course content untuk course tertentu
    """
    user_id = request.auth['id']
    
    # Check if the user is enrolled in the course
    course_member = CourseMember.objects.filter(course_id=course_id, user_id=user_id)
    if not course_member.exists():
        return {"detail": "You are not enrolled in this course."}, 403
    
    # Retrieve the course content data
    course_contents = CourseContent.objects.filter(course_id=course_id)
    
    return course_contents

@apiv1.get('courses/{id}', response=DetailCourseOut)
def detailCourse(request, id:int):
    course = Course.objects.get(pk=id)
    return course


