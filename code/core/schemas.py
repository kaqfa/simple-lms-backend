from ninja import Schema, ModelSchema, FilterSchema, Field
from datetime import datetime
from typing import Optional, List

from .models import Course, User, CourseMember, CourseContent

class Register(Schema):
    username: str
    password: str
    email: str
    first_name: str
    last_name: str

class CalcIn(Schema):
    nil1: int
    nil2: int
    opr: str
    hasil: int = 0

    def calcHasil(self):
        hasil = self.nil1 + self.nil2
        if self.opr == '-':
            hasil = self.nil1 - self.nil2
        elif self.opr == 'x':
            hasil = self.nil1 * self.nil2
        
        return {'nilai1': self.nil1, 'nilai2': self.nil2, 
                'operator': self.opr, 'hasil': self.hasil}

class UserOut(Schema):
    id: int
    username: str
    first_name: str
    last_name: str
    email: str


class CourseDetailOut(Schema):
    id: int
    name: str
    description: str
    price: int
    image: Optional[str] = ''
    teacher: UserOut
    created_at: datetime
    updated_at: datetime
    num_members: int
    num_contents: int


class CourseOut(Schema):
    id: int
    name : str
    description : str
    price : int
    image : Optional[str] = ''
    teacher : UserOut
    created_at : datetime
    updated_at : datetime

class CourseMemberOut(Schema):
    id: int
    course_id: CourseOut
    roles: str
    created_at: datetime

from pydantic import BaseModel
from datetime import datetime
from typing import Optional

class CourseContentSchema(BaseModel):
    """
    A schema for representing the course content model.

    Attributes:
        id (int): The unique identifier of the course content.
        name (str): The title of the course content.
        description (str): A brief description of the course content.
        video_url (Optional[str]): The URL of the video content.
        file_attachment (Optional[str]): The file attachment URL.
        course_id (int): The identifier of the associated course.
        parent_id (Optional[int]): The identifier of the parent content.
        created_at (datetime): The timestamp when the content was created.
        updated_at (datetime): The timestamp when the content was last updated.
    """
    id: int
    name: str
    description: str
    video_url: Optional[str] = None
    file_attachment: Optional[str] = None
    course_id: CourseOut
    parent_id: Optional[int] = None
    created_at: datetime
    updated_at: datetime

from django.db.models import Q

class CourseFilter(FilterSchema):
    price: Optional[int] = 0
    created_at: Optional[datetime] = None
    search : Optional[str] = Field(None, q=['name__icontains', 'description__icontains'])

    def filter_price(self, value: int):
        return Q(price__gt=value)
    
    def filter_created_at(self, value: datetime):        
        return Q(created_at__gt=value) if value else Q()
    
class ContentTitleOut(Schema):
    id: int
    name: str

class DetailCourseOut(CourseOut):
    contents: List[ContentTitleOut] = Field(alias="coursecontent_set")

class CommentIn(Schema):
    comment: str
    content_id: int
