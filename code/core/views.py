from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User
from core.models import Course, CourseMember
from django.http import JsonResponse
from django.core import serializers


# Create your views here.
def index(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return render(request, 'index.html', {"ip": ip})

def testing(request):
    user_test = User.objects.filter(username="usertesting2")    
    if not user_test.exists():
        user_test = User.objects.create_user(
                            username="usertesting2", 
                            email="usertest2@email.com", 
                            password="seadanyaaja2")        
    all_users = serializers.serialize('python', User.objects.all())

    admin = User.objects.get(pk=1)
    user_test.delete()

    after_delete = serializers.serialize('python', User.objects.all())

    response = {
            "admin_user": serializers.serialize('python', [admin])[0],
            "all_users" : all_users,
            "after_del" : after_delete,
        }
    return JsonResponse(response)
