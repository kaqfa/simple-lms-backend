from django.conf import settings
import mimetypes

DEBUG = True
ALLOWED_HOSTS = ['*']

# settings.DATABASES['default'] = {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'simple_lms',
#         'USER': 'simple_user',
#         'PASSWORD': 'simple_password',
#         'HOST': 'simple_db',
#     }

INTERNAL_IPS = [    
    "127.0.0.1",
    "192.168.65.1",
]

settings.INSTALLED_APPS += [
    'core',
    "debug_toolbar"
]

# mimetypes.add_type("application/javascript", ".js", True)

# DEBUG_TOOLBAR_CONFIG = {
#     "INTERCEPT_REDIRECTS" : False,
# }

LOGGING = {
    'version': 1,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'DEBUG',
            'handlers': ['console'],
        }
    }
}